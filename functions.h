#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <cstring>

namespace itandroids {

    int max (int &num1, int &num2);

    void swap (int &num1, int &num2);

    float pow (float &num1, float &num2);

    void printArray (char arr[]);
}

    void printArray (char arr[]);